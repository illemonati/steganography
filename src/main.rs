mod methods;

use std::fs;
use std::fs::{File, OpenOptions};
use std::io::{Read, Write};
use std::str::FromStr;
use clap::Parser;
use crate::methods::{Method, MethodMode};
use crate::methods::lsb::LSBMethod;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Input image path
    #[arg(short, long)]
    image_input: String,

    /// Input data file path
    #[arg(short, long, default_value_t = String::from("file.txt"))]
    file: String,

    /// encode mode
    #[arg(short, long)]
    encode: bool,

    /// decode mode
    #[arg(short, long)]
    decode: bool,

    /// decode truncates before 100 continuous zero bytes
    #[arg(short, long, default_value_t=true)]
    zero_truncate_mode: bool,

    /// Image type
    #[arg(short, long, default_value_t = String::from("rgb8"))]
    _type: String,

    /// Output file path
    #[arg(short, long)]
    output: Option<String>,


}

fn main() {
    let args = Args::parse();

    let image_type = MethodMode::from_str(&args._type).expect("Valid types: rgb8 | rgba8");

    if args.encode {
        let output = args.output.unwrap_or(String::from("out.png"));
        println!("Encoding {} into {} with {} and saving as {}", args.file, args.image_input, args._type, output);

        let img = image::open(args.image_input).expect("Unable to open image");
        let data = get_file_as_byte_vec(&args.file);

        let result = LSBMethod::new(image_type).encode(img, &data).expect("Unable to parse image");

        result.save(output).expect("Unable to save image");
    } else if args.decode {

        let img = image::open(args.image_input).expect("Unable to open image");
        let result = LSBMethod::new(image_type).decode(img);
        let mut truncated_result: &[u8] = &result;

        if args.zero_truncate_mode {
            let mut zeros = 0;
            for i in 0..result.len() {
                if result[i] == 0 {
                    zeros += 1;
                    if zeros == 20 {
                        truncated_result = &result[0..=i-20];
                    }
                } else {
                    zeros = 0;
                }
            }
        }

        let output = args.output.unwrap_or(String::from("out.txt"));

        let mut file = OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(output)
            .expect("Unable to open file");

        file.write_all(truncated_result).expect("Unable to write result");

    } else {
        println!("Specify at least one of encode | decode");
    }
}

fn get_file_as_byte_vec(filename: &str) -> Vec<u8> {
    let mut f = File::open(&filename).expect("Input data file not found");
    let metadata = fs::metadata(&filename).expect("Unable to read metadata for input data file");
    let mut buffer = vec![0; metadata.len() as usize];
    f.read(&mut buffer).expect("buffer overflow");

    buffer
}
