use image::{DynamicImage, RgbaImage, RgbImage};
use crate::methods::{Method, MethodMode};
use bitvec::prelude::*;

pub struct LSBMethod {
    mode: MethodMode
}

impl Method for LSBMethod {
    fn new( mode: MethodMode) -> Self {
        Self {
            mode
        }
    }

    fn encode(&self, image: DynamicImage, data: &[u8]) -> Option<DynamicImage> {
        let res = match self.mode {
            MethodMode::Rgb8 => self.encode_rgb8(image.to_rgb8(), data),
            MethodMode::Rgba8 => self.encode_rgba8(image.to_rgba8()),
        };
        Some(res)
    }

    fn decode(&self, image: DynamicImage) -> Vec<u8> {
        match self.mode {
            MethodMode::Rgb8 => self.decode_rgb8(image.to_rgb8()),
            MethodMode::Rgba8 => self.decode_rgba8(image.to_rgba8()),
        }
    }
}

impl LSBMethod {
    fn encode_rgb8(&self, mut image: RgbImage, data: &[u8]) -> DynamicImage {
        let (width, height) = image.dimensions();
        let mut data_index = 0;
        let data_bits = data.view_bits::<Lsb0>();
        for x in 0..width {
            for y in 0..height {
                let image::Rgb(pixel_data) = image.get_pixel_mut(x, y);
                for i in 0..3 {
                    if data_index < data_bits.len() {
                        pixel_data[i].view_bits_mut::<Lsb0>().set(0, data_bits[data_index]);
                        data_index += 1;
                    } else {
                        pixel_data[i].view_bits_mut::<Lsb0>().set(0, false);
                    }

                }
                // image.put_pixel(x, y, image::Rgb(*pixel_data));
            }
        }
        DynamicImage::from(image)
    }

    fn decode_rgb8(&self, mut image: RgbImage) -> Vec<u8> {
        let (width, height) = image.dimensions();
        let mut data_index = 0;
        let mut arr = BitVec::<u8, Lsb0>::repeat(false, (width * height * 3) as usize);
        for x in 0..width {
            for y in 0..height {
                let image::Rgb(pixel_data) = image.get_pixel_mut(x, y);
                for i in 0..3 {
                    arr.set(data_index,pixel_data[i].view_bits_mut::<Lsb0>()[0]);
                    data_index += 1;
                }

            }
        }
        arr.into_vec()
    }

    fn decode_rgba8(&self, mut image: RgbaImage) -> Vec<u8> {
        todo!()
    }

    fn encode_rgba8(&self, mut image: RgbaImage) -> DynamicImage {
        DynamicImage::from(image)
    }
}