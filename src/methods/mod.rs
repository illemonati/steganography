pub mod lsb;

use std::fmt::{Display, Formatter};
use std::str::FromStr;
use image::{DynamicImage};
use crate::methods::MethodMode::{Rgb8, Rgba8};

pub enum MethodMode {
    Rgb8,
    Rgba8,
}

impl Display for MethodMode {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            MethodMode::Rgb8 => write!(f, "Rgb8"),
            MethodMode::Rgba8 => write!(f, "Rgba8"),
        }
    }
}

impl FromStr for MethodMode {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "rgb8" => Ok(Rgb8),
            "rgba8" => Ok(Rgba8),
            _ => Err(())
        }
    }
}

pub trait Method {
    fn new(mode: MethodMode) -> Self;
    fn encode(&self, image: DynamicImage, data: &[u8]) -> Option<DynamicImage>;
    fn decode(&self, image: DynamicImage) -> Vec<u8>;
}